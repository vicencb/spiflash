#include "bitbang.h"
#include "main.h"
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

enum {CS, CLK, MOSI, MISO, NUM_GPIOS, NUM_GPIOS_PER_CHIP = 32};

// SysFS GPIO is ~30% slower

#ifdef USE_SYSFS_GPIO
static struct {
  char *name  ;
  long  num   ;
  int   fd    ;
  int   req   ;
  char  dir[4];
} gpio[NUM_GPIOS] = {
  [CS  ] = {.name = "cs"  , .dir="out", .fd = -1},
  [CLK ] = {.name = "clk" , .dir="out", .fd = -1},
  [MOSI] = {.name = "mosi", .dir="out", .fd = -1},
  [MISO] = {.name = "miso", .dir="in" , .fd = -1},
};

static void release(void) {
  char     buf[16];
  ssize_t  len    ;
  int      fd     ;
  int      i      ;
  int      any = 0;

  for (i = 0; i < NUM_GPIOS; ++i) {
    any |= gpio[i].req;
    if (gpio[i].fd >= 0) {
      close(gpio[i].fd);
      gpio[i].fd = -1;
    }
  }

  if (any) {
    fd = open("/sys/class/gpio/unexport", O_WRONLY);
    if (fd < 0) {
      fprintf(stderr, "Error openning /sys/class/gpio/unexport: %s\n", strerror(errno));
      return;
    }

    for (i = 0; i < NUM_GPIOS; ++i) {
      if (gpio[i].req) {
        gpio[i].req = 0;
        len = 1 + snprintf(buf, sizeof(buf), "%li", gpio[i].num);
        if (write(fd, buf, len) != len) {
          fprintf(stderr, "Error releasing gpio %li: %s\n", gpio[i].num, strerror(errno));
        }
      }
    }

    close(fd);
  }
}

static void request_bus(void) {
  char     buf[64];
  ssize_t  len    ;
  int      fd     ;
  int      i      ;

  fd = open("/sys/class/gpio/export", O_WRONLY);
  if (fd < 0) {
    fprintf(stderr, "Error openning /sys/class/gpio/export: %s\n", strerror(errno));
    exit(1);
  }

  atexit(release);

  for (i = 0; i < NUM_GPIOS; ++i) {
    len = 1 + snprintf(buf, sizeof(buf), "%li", gpio[i].num);
    if (write(fd, buf, len) != len) {
      fprintf(stderr, "Error requesting gpio %li: %s\n", gpio[i].num, strerror(errno));
      exit(1);
    }
    gpio[i].req = 1;
  }

  close(fd);

  for (i = 0; i < NUM_GPIOS; ++i) {
    snprintf(buf, sizeof(buf), "/sys/class/gpio/gpio%li/value", gpio[i].num);
    fd = open(buf, O_RDWR);
    if (fd < 0) {
      fprintf(stderr, "Error openning %s: %s\n", buf, strerror(errno));
      exit(1);
    }
    gpio[i].fd = fd;
  }

  for (i = 0; i < NUM_GPIOS; ++i) {
    snprintf(buf, sizeof(buf), "/sys/class/gpio/gpio%li/direction", gpio[i].num);
    fd = open(buf, O_WRONLY);
    if (fd < 0) {
      fprintf(stderr, "Error openning %s: %s\n", buf, strerror(errno));
      exit(1);
    }
    if (write(fd, gpio[i].dir, 4) != 4) {
      fprintf(stderr, "Error configuring gpio %li as %s: %s\n", gpio[i].num, gpio[i].dir, strerror(errno));
      exit(1);
    }
    close(fd);
  }
}

static void set_gpio(int i, int val) {
  if (write(gpio[i].fd, val ? "1" : "0", 2) != 2) {
    fprintf(stderr, "Error setting gpio %li to %i: %s\n", gpio[i].num, !!val, strerror(errno));
    exit(1);
  }
}

static int get_miso(void) {
  char buf[2];

  int err = !!lseek(gpio[MISO].fd, 0, SEEK_SET);
  err = err || (read(gpio[MISO].fd, buf, 2) != 2);
  err = err || (buf[0] != '0' && buf[0] != '1'  );
  err = err || (buf[1] != '\n'                  );
  if (err) {
    fprintf(stderr, "Error getting gpio %li: %s\n", gpio[MOSI].num, strerror(errno));
    exit(1);
  }

  return buf[0] & 1;
}

#else
#include <linux/gpio.h>
#include <sys/ioctl.h>

static struct {
  char    *name;
  long     num;
  int      fd;
  unsigned val;
  unsigned dir;
} gpio[NUM_GPIOS] = {
  [CS  ] = {.name = "cs"  , .dir = GPIOHANDLE_REQUEST_OUTPUT, .val = 1},
  [CLK ] = {.name = "clk" , .dir = GPIOHANDLE_REQUEST_OUTPUT, .val = 0},
  [MOSI] = {.name = "mosi", .dir = GPIOHANDLE_REQUEST_OUTPUT, .val = 0},
  [MISO] = {.name = "miso", .dir = GPIOHANDLE_REQUEST_INPUT           },
};

static void request_bus(void) {
  char                      buf[64];
  struct gpiochip_info      info;
  struct gpiohandle_request req = {.consumer_label = "spiflash programmer", .lines = 1};

  for (unsigned i = 0; i < NUM_GPIOS; ++i) {
    unsigned gpio_chip   = gpio[i].num / NUM_GPIOS_PER_CHIP;
    unsigned gpio_offset = gpio[i].num - gpio_chip * NUM_GPIOS_PER_CHIP;
    snprintf(buf, sizeof(buf), "/dev/gpiochip%u", gpio_chip);
    int fd = open(buf, 0);
    if (fd < 0) {
      fprintf(stderr, "Error openning %s: %s\n", buf, strerror(errno));
      exit(1);
    }
    int ret = ioctl(fd, GPIO_GET_CHIPINFO_IOCTL, &info);
    if (ret == -1) {
      fprintf(stderr, "Error ioctl %s: %s\n", buf, strerror(errno));
      exit(1);
    }
    if (info.lines != NUM_GPIOS_PER_CHIP) {
      fprintf(stderr, "Unexpected NUM_GPIOS_PER_CHIP on %s: %u\n", buf, info.lines);
      exit(1);
    }
    req.flags             = gpio[i].dir;
    req.lineoffsets   [0] = gpio_offset;
    req.default_values[0] = gpio[i].val;
    ret = ioctl(fd, GPIO_GET_LINEHANDLE_IOCTL, &req);
    if (ret == -1 || req.fd <= 0) {
      fprintf(stderr, "Error ioctl %s: %s\n", buf, strerror(errno));
      exit(1);
    }
    gpio[i].fd = req.fd;
    close(fd);
  }
}

static void set_gpio(int i, int val) {
  struct gpiohandle_data data;
  data.values[0] = !!val;
  if (ioctl(gpio[i].fd, GPIOHANDLE_SET_LINE_VALUES_IOCTL, &data) == -1) {
    fprintf(stderr, "Error setting gpio %li to %i: %s\n", gpio[i].num, !!val, strerror(errno));
    exit(1);
  }
}

static int get_miso(void) {
  struct gpiohandle_data data;
  if (ioctl(gpio[MISO].fd, GPIOHANDLE_GET_LINE_VALUES_IOCTL, &data) == -1) {
    fprintf(stderr, "Error getting gpio %li: %s\n", gpio[MOSI].num, strerror(errno));
    exit(1);
  }
  return !!data.values[0];
}
#endif

static void set_cs  (int val) { set_gpio(CS  , val); }
static void set_sck (int val) { set_gpio(CLK , val); }
static void set_mosi(int val) { set_gpio(MOSI, val); }

static const struct bitbang bitbang = {
  .set_cs   = set_cs  ,
  .set_sck  = set_sck ,
  .set_mosi = set_mosi,
  .get_miso = get_miso,
};

static void usage(void) {
  fprintf(stdout,
    "programmer parameter should be: bitbang,linux,cs=X,clk=X,mosi=X,miso=X\n"
    "    Where each pin function should be assigned a linux gpio number.\n"
  );
  exit(1);
}

struct bitbang const *bitbang_linux(void) {
  if (get_param("help", 0, 0, 0) != PARAM_NOT_FOUND) { usage(); }
  for (unsigned i = 0; i < NUM_GPIOS; ++i) {
    if (get_param(gpio[i].name, &gpio[i].num, 0, 0) != PARAM_SUCCESS || gpio[i].num < 0) {
      fprintf(stderr, "Missing gpio number for %s\n", gpio[i].name);
      usage();
    }
  }

  request_bus();

  return &bitbang;
}
