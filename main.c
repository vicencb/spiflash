#include "main.h"
#include "flash.h"
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

static char *programmer;
static struct flash const *flash;

enum param_code get_param(char *param, long *num, char **str, unsigned *len) {
  int ret = PARAM_NOT_FOUND;

  size_t const l = strlen(param);
  char *x = programmer;
  while (1) {
    char *y, *z, *p;
    if (!x || !*x) { return ret; }
    for (z = x;    *z && *z != ','; ++z);
    for (y = x; y < z && *y != '='; ++y);
    if (x + l == y && memcmp(param, x, l) == 0) {
      if (ret != PARAM_NOT_FOUND) {
        return PARAM_MULTIPLE;
      } else if (y == z) {
        ret = PARAM_NO_VALUE;
      } else {
        ++y;
        if (num) {
          unsigned base = y[0] == '0' && ((y[1] | ' ') == 'x') ? 16 : 10;
          errno = 0;
          long val = strtol(y, &p, base);
          if ((*p && *p != ',') || errno) { return PARAM_VALUE_ERR; }
          *num = val;
        } else if (str && len) {
          *str = y;
          *len = z - y;
        }
        ret = PARAM_SUCCESS;
      }
    }
    x = z + !!*z;
  }
}

#if RK3328
#define BBL_PROG "bitbang,linux,cs=104,clk=96,mosi=97,miso=98"
#else // RK3399
#define BBL_PROG "bitbang,linux,cs=42,clk=41,mosi=40,miso=39"
#endif

static void usage(void) {
  fprintf(stdout,
    "Usage: spiflash -p <programmer> [ -o | -z ] [ -m ] [ -n <size> ] [ -r <file> | -w <file> | -v <file> | -e ]\n"
    "  -p: select flash programmer:\n"
    "      <programmer name>,help\n"
    "      " BBL_PROG "\n"
    "      linux,bus=0,cs=0\n"
#ifdef FTDI
    "      ftdi[,vid=0x0403][,pid=0x6010][,desc=Dual RS232-HS][,serial=abcde][,index=0][,intf=A][,cs=3][,l0=0]\n"
#endif
    "  -o: pad input file with ones\n"
    "  -z: pad input file with zeros\n"
    "  -m: pad imput file only to the next multiple of the block size\n"
    "  -n: do not read SFDP, use provided size (in bytes)\n"
    "  -r: read flash into file\n"
    "  -w: write flash from file\n"
    "  -v: verify flash with file\n"
    "  -e: erase flash\n"
  );
  exit(1);
}

static void sighandler(int signum) {
  if (flash) { flash->wren(0); }
  exit(1); // Need a clean exit() for atexit() to work.
  (void)signum;
}

static void diff(unsigned addr, uint8_t const *a, uint8_t const *b) {
  unsigned const line = 16;
  fprintf(stdout, "Addr   %-*s %-*s\n", 3 * line, "Reference", 3 * line, "Contents");
  for (unsigned i = 0; i < FLASH_BLOCK_SIZE; i += line) {
    fprintf(stdout, "%06X", addr + i);
    for (unsigned j = 0; j < line; ++j) { fprintf(stdout, " %02X", a[i + j]); }
    fprintf(stdout, " ");
    for (unsigned j = 0; j < line; ++j) {
      unsigned k = i + j;
      unsigned l = a[k] != b[k];
      if (l) { fprintf(stdout, "\x1B[1m"); }
      fprintf(stdout, " %02X", b[k]);
      if (l) { fprintf(stdout, "\x1B[0m"); }
    }
    fprintf(stdout, "\n");
  }
}

static void inline_error(char const *msg, unsigned addr, uint8_t const *a, uint8_t const *b) {
  fprintf(stdout, "X\n");
  fflush(stdout);
  fprintf(stderr, "%s\n", msg);
  diff(addr, a, b);
  exit(1);
}

static void do_work(unsigned size, int op, uint8_t const *file_data) {
  uint8_t *flash_data = malloc(FLASH_BLOCK_SIZE);
  if (!flash_data) {
    fprintf(stderr, "Malloc error: %s\n", strerror(errno));
    exit(1);
  }

  enum {
    ERASE_ATTEMPT1, ERASE_ATTEMPT2, ERASE_ATTEMPT3,
    WRITE_ATTEMPT1, WRITE_ATTEMPT2, WRITE_ATTEMPT3,
    TOO_MAMY_ATTEMPTS,
  } attempt = ERASE_ATTEMPT1;
  unsigned erase_write = 0;

  switch (op) {
  case 'v': {
    fprintf(stdout, "Letter=4KB V=equal X=error\n");
  } break;
  case 'e': {
    fprintf(stdout, "Letter=4KB V=empty E=erase X=error .=erase attempt\n");
  } break;
  case 'w': {
    fprintf(stdout,
      "Letter=4KB V=equal E=erase W=write M=modify(erase&write)\n"
      "X=error .=erase attempt :=write attempt ;=modify attempt\n"
    );
  } break;
  }

  for (unsigned addr = 0; addr < size; addr += FLASH_BLOCK_SIZE) {
  again:
    fflush(stdout);
    flash->read(addr, flash_data);
    switch (op) {
    case 'v': {
      if (memcmp(file_data + addr, flash_data, FLASH_BLOCK_SIZE)) {
        inline_error("Data mismatch", addr, file_data + addr, flash_data);
      }
      fprintf(stdout, "V");
    } break;
    case 'w':
    case 'e': {
      uint8_t erase_needed = 0;
      uint8_t write_needed = 0;
      for (unsigned i = 0; i < FLASH_BLOCK_SIZE && !erase_needed; ++i) {
        uint8_t a = file_data[addr + i];
        uint8_t b = flash_data[i];
        erase_needed  = a & ~b;
        write_needed |= b & ~a;
      }
      if (erase_needed) {
        if (attempt > WRITE_ATTEMPT1) {
          fprintf(stdout, attempt > WRITE_ATTEMPT1 ? ";" : ".");
          inline_error("Modify failure", addr, file_data + addr, flash_data);
        }
        if (attempt > ERASE_ATTEMPT1) { fprintf(stdout, "."); }
        if (attempt > ERASE_ATTEMPT3) {
          inline_error("Erase failure", addr, file_data + addr, flash_data);
        }
        flash->wren(1);
        flash->erase(addr);
        usleep(50000);
        while (flash->status() & FLASH_WIP) { usleep(10000); }
        flash->wren(0);
        erase_write |= 1;
        ++attempt;
        goto again;
      }
      if (attempt < WRITE_ATTEMPT1) { attempt = WRITE_ATTEMPT1; }
      if (write_needed) {
        if (attempt > WRITE_ATTEMPT1) { fprintf(stdout, ":"); }
        if (attempt > WRITE_ATTEMPT3) {
          inline_error("Write failure", addr, file_data + addr, flash_data);
        }
        for (unsigned i = 0; i < FLASH_BLOCK_SIZE; i += FLASH_PAGE_SIZE) {
          uint8_t write_page_needed = 0;
          for (unsigned j = 0; j < FLASH_PAGE_SIZE && !write_page_needed; ++j) {
            uint8_t a = file_data[addr + i + j];
            uint8_t b = flash_data[i + j];
            write_page_needed = b & ~a;
          }
          if (write_page_needed) {
            flash->wren(1);
            flash->write(addr + i, file_data + addr + i);
            usleep(1000);
            while (flash->status() & FLASH_WIP) { usleep(200); }
          }
        }
        flash->wren(0);
        erase_write |= 2;
        ++attempt;
        goto again;
      }
      switch (erase_write) {
      case 0: { fprintf(stdout, "V"); } break;
      case 1: { fprintf(stdout, "E"); } break;
      case 2: { fprintf(stdout, "W"); } break;
      case 3: { fprintf(stdout, "M"); } break;
      }
      attempt = ERASE_ATTEMPT1;
      erase_write = 0;
    } break;
    }
  }
  fprintf(stdout, "\n");
  free(flash_data);
}

enum padding { PAD_NO, PAD_ONES, PAD_ZEROS };

int main(int argc, char **argv) {
  int          op       =  0;
  char        *file     =  0;
  enum padding padding  = PAD_NO;
  unsigned     padmlt   =  0;
  struct flash_sfdp skipsfdp = {};

  while (1) {
    ++argv;
    --argc;
    if (!argc) { break; }
    if (argv[0][0] != '-' || !argv[0][1] || argv[0][2]) { usage(); }
    switch (argv[0][1]) {
    default : {
      usage();
    } break;
    case 'p': {
      ++argv;
      --argc;
      if (!argc || programmer) { usage(); }
      programmer = *argv;
    } break;
    case 'n': {
      ++argv;
      --argc;
      if (!argc || skipsfdp.size) { usage(); }
      char *endptr;
      unsigned long size = strtoul(argv[0], &endptr, (argv[0][1] | ' ') == 'x' ? 16 : 10);
      skipsfdp.size = size;
      if (endptr[0]) { usage(); }
      if (size < FLASH_BLOCK_SIZE || size > 0x1000000 || (size & (size - 1))) {
        fprintf(stderr, "Unsupported size 0x%08lX\n", size);
        exit(1);
      }
    } break;
    case 'r': {
      ++argv;
      --argc;
      if (!argc || op) { usage(); }
      op = 'r';
      file = *argv;
    } break;
    case 'w': {
      ++argv;
      --argc;
      if (!argc || op) { usage(); }
      op = 'w';
      file = *argv;
    } break;
    case 'v': {
      ++argv;
      --argc;
      if (!argc || op) { usage(); }
      op = 'v';
      file = *argv;
    } break;
    case 'e': {
      if (op) { usage(); }
      op = 'e';
    } break;
    case 'm': {
      padmlt = 1;
    } break;
    case 'o': {
      if (padding != PAD_NO) { usage(); }
      padding = PAD_ONES;
    } break;
    case 'z': {
      if (padding != PAD_NO) { usage(); }
      padding = PAD_ZEROS;
    } break;
    }
  }

  {
    struct sigaction sigact = {};
    sigact.sa_handler = sighandler;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;
    sigaction(SIGINT , &sigact, NULL);
    sigaction(SIGTERM, &sigact, NULL);
    sigaction(SIGQUIT, &sigact, NULL);
    sigaction(SIGPIPE, &sigact, NULL);
  }

  flash = flash_spi();
  if (!flash) { usage(); }

  unsigned id = flash->id();
  if (id == 0 || id == ~0U) {
    fprintf(stderr, "Invalid chip ID 0x%08X\n", id);
    exit(1);
  }
  fprintf(stdout, "Chip ID is 0x%08X\n", id);

  unsigned status = flash->status();
  if (status & FLASH_WIP) {
    fprintf(stdout, "Flash is busy, waiting...\n");
    for (unsigned i = 1000; i && (status & FLASH_WIP); --i) {
      usleep(100000);
      status = flash->status();
    }
    if (status & FLASH_WIP) {
      fprintf(stderr, "Flash is busy\n");
      exit(1);
    }
  }
  if (status & FLASH_WEL) {
    flash->wren(0);
  }

  struct flash_sfdp const *sfdp = &skipsfdp;
  if (!skipsfdp.size) { sfdp = flash->sfdp(); }
  fprintf(stdout, "Flash size is 0x%04X (%u 4KB-blocks)\n", sfdp->size, sfdp->size / FLASH_BLOCK_SIZE);

  if (!op) { return 0; }

  uint8_t *file_data = malloc(sfdp->size);
  if (!file_data) {
    fprintf(stderr, "Malloc error: %s\n", strerror(errno));
    exit(1);
  }
  memset(file_data, 0xFF, sfdp->size);

  if (op == 'r') {
    int fd = open(file, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
    if (fd < 0) {
      fprintf(stderr, "Error creating file %s: %s\n", file, strerror(errno));
      exit(1);
    }

    fprintf(stdout, "R=read 4KB\n");
    for (unsigned addr = 0; addr < sfdp->size; addr += FLASH_BLOCK_SIZE) {
      flash->read(addr, file_data + addr);
      fprintf(stdout, "R");
      fflush(stdout);
    }
    fprintf(stdout, "\n");

    ssize_t len = write(fd, file_data, sfdp->size);
    if (len < 0 || (uint64_t)len != sfdp->size) {
      fprintf(stderr, "Error writing file %s: %s\n", file, strerror(errno));
      exit(1);
    }
    int ret = close(fd);
    if (ret) {
      fprintf(stderr, "Error closing file %s: %s\n", file, strerror(errno));
      exit(1);
    }

    return 0;
  }

  unsigned work_size = sfdp->size;
  if (op == 'v' || op == 'w') {
    int fd = open(file, O_RDONLY);
    if (fd < 0) {
      fprintf(stderr, "Error openning file %s: %s\n", file, strerror(errno));
      exit(1);
    }

    struct stat statbuf;
    int ret = fstat(fd, &statbuf);
    if (ret || statbuf.st_size < 0) {
      fprintf(stderr, "Error stating file %s: %s\n", file, strerror(errno));
      exit(1);
    }
    uint64_t fd_sz = statbuf.st_size;
    if (fd_sz > sfdp->size) {
      fprintf(stderr, "Error file %s is bigger than flash: %lu > %u\n", file, fd_sz, sfdp->size);
      exit(1);
    }
    if (fd_sz != sfdp->size && padding == PAD_NO) {
      fprintf(stderr, "Error file %s do not match flash size: %lu <> %u\n", file, fd_sz, sfdp->size);
      exit(1);
    }

    ssize_t siz = fd_sz < sfdp->size ? fd_sz : sfdp->size;
    ssize_t len = read(fd, file_data, siz);
    if (len != siz) {
      fprintf(stderr, "Error reading file %s: %s\n", file, strerror(errno));
      exit(1);
    }

    ret = close(fd);
    if (ret) {
      fprintf(stderr, "Error closing file %s: %s\n", file, strerror(errno));
      exit(1);
    }

    switch (padding) {
    case PAD_ONES : { memset(file_data + siz, 0xFF, sfdp->size - siz); } break;
    case PAD_ZEROS: { memset(file_data + siz, 0x00, sfdp->size - siz); } break;
    case PAD_NO   : {} break;
    }
    if (padding != PAD_NO && padmlt) {
      work_size = ((fd_sz + (FLASH_BLOCK_SIZE - 1)) / FLASH_BLOCK_SIZE) * FLASH_BLOCK_SIZE;
    }
  }

  do_work(work_size, op, file_data);

  free(file_data);
  return 0;
}
