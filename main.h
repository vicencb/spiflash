#ifndef MAIN_H
#define MAIN_H

enum param_code {
  PARAM_SUCCESS,
  PARAM_NO_VALUE,
  PARAM_NOT_FOUND,
  PARAM_MULTIPLE,
  PARAM_VALUE_ERR,
};

enum param_code get_param(char *param, long *num, char **str, unsigned *len)
  __attribute__((nonnull(1)));

#endif
