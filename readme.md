ABOUT
=====

This is a minimalist SPI flash programmer.

It supports

 1. identifying
 2. reading
 3. verifying
 4. erasing
 5. writing

small SPI flash chips that implement the SFDP command through

 1. Linux SPIDEV
 2. Linux GPIO

interfaces.

LICENSE
=======

All files in this directory are licensed under the 0BSD license (BSD Zero Clause License).

https://spdx.org/licenses/0BSD.html
