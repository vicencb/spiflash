#ifndef FLASH_H
#define FLASH_H

#include <stdint.h>

enum {
  FLASH_BLOCK_SIZE = 0x1000,
  FLASH_PAGE_SIZE  = 0x100,
  FLASH_WIP = 1 << 0,
  FLASH_WEL = 1 << 1,
};

struct flash_sfdp {
  unsigned size;
  unsigned erase_op;
};

struct flash {
  unsigned                 (*id    )(void);
  unsigned                 (*status)(void);
  struct flash_sfdp const *(*sfdp  )(void);
  void                     (*read  )(unsigned addr, uint8_t *);
  void                     (*write )(unsigned addr, uint8_t const *);
  void                     (*wren  )(unsigned);
  void                     (*erase )(unsigned addr);
};

struct flash const *flash_spi(void);

#endif
