#include "spi.h"
#include "main.h"
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/ioctl.h>
#include <linux/spi/spidev.h>
#include <linux/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

static char    spi_bus_name[32];
static ssize_t spi_bus_len;
static int     fd;

static void cmd(unsigned wcnt, unsigned rcnt, uint8_t const *warr, uint8_t *rarr) {
  unsigned i = rcnt ? 2 : 1;
  struct spi_ioc_transfer msg[2] = {
    {.tx_buf = (uint64_t)warr, .len = wcnt},
    {.rx_buf = (uint64_t)rarr, .len = rcnt},
  };
  if (ioctl(fd, SPI_IOC_MESSAGE(i), msg) == -1) {
    fprintf(stderr, "Error writing to spi driver: %s\n", strerror(errno));
    exit(1);
  }
}

static const struct spi spi = {
  .cmd = cmd,
};

static void unbind_driver(char const *drv, int ask) {
  char reply = 0;
  if (ask) {
    fprintf(stdout, "%s is taken by driver %s.\nDo you want to unbind it? (y/n) ", spi_bus_name, drv);
    fflush(stdout);
    scanf("%c%*c", &reply);
  } else {
    reply = 'y';
  }
  if ((reply | ' ') != 'y') { return; }
  char str[256];
  snprintf(str, sizeof(str), "/sys/bus/spi/drivers/%s/unbind", drv);
  int fd = open(str, O_WRONLY);
  if (fd < 0) {
    fprintf(stderr, "Error openning %s: %s\n", str, strerror(errno));
    exit(1);
  }
  if (write(fd, spi_bus_name, spi_bus_len) != spi_bus_len) {
    fprintf(stderr, "Error writing %s: %s\n", str, strerror(errno));
    exit(1);
  }
  close(fd);
}

static void overwrite_driver(int enable) {
  char str[256];
  snprintf(str, sizeof(str), "/sys/bus/spi/devices/%s/driver_override", spi_bus_name);
  int fd = open(str, O_WRONLY);
  if (fd < 0) {
    fprintf(stderr, "Error openning %s: %s\n", str, strerror(errno));
    exit(1);
  }
  char const *driver = enable ? "spidev" : "";
  ssize_t     len    = enable ? 7 : 1;
  if (write(fd, driver, len) != len) {
    fprintf(stderr, "Error writing %s: %s\n", str, strerror(errno));
    exit(1);
  }
  close(fd);
}

static void bind_driver(void) {
  static char const *const str = "/sys/bus/spi/drivers/spidev/bind";
  int fd = open(str, O_WRONLY);
  if (fd < 0) {
    fprintf(stderr, "Error openning %s: %s\n", str, strerror(errno));
    fprintf(stderr, "You may need to 'modprobe spidev'\n");
    exit(1);
  }
  if (write(fd, spi_bus_name, spi_bus_len) != spi_bus_len) {
    fprintf(stderr, "Error writing %s: %s\n", str, strerror(errno));
    exit(1);
  }
  close(fd);
}

static void unbind_spidev(void) {
  unbind_driver("spidev", 0);
  overwrite_driver(0);
}

static void bind_spidev(void) {
  DIR *drivers = opendir("/sys/bus/spi/drivers");
  if (!drivers) {
    fprintf(stderr, "Error openning /sys/bus/spi/drivers: %s\n", strerror(errno));
    exit(1);
  }
  struct dirent *ent;
  while (1) {
    errno = 0;
    ent = readdir(drivers);
    if (errno) {
      fprintf(stderr, "Error reading /sys/bus/spi/drivers: %s\n", strerror(errno));
      exit(1);
    }
    if (!ent) { break; }
    if (strcmp(ent->d_name, "spidev")) {
      char str[320];
      struct stat statbuf;
      snprintf(str, sizeof(str), "/sys/bus/spi/drivers/%s/%s", ent->d_name, spi_bus_name);
      if (!lstat(str, &statbuf)) {
        unbind_driver(ent->d_name, 1);
      } else if (errno != ENOENT) {
        fprintf(stderr, "Error checking %s: %s\n", str, strerror(errno));
        exit(1);
      }
    }
  }
  closedir(drivers);
  atexit(unbind_spidev);
  overwrite_driver(1);
  bind_driver();
}

static void usage(void) {
  fprintf(stdout,
    "programmer parameter should be: linux,bus=X,cs=Y\n"
    "    Where bus and cs come from the device name '/dev/spidevX.Y'\n"
  );
  exit(1);
}

struct spi const *spi_linux(void) {
  long bus, cs;
  if (get_param("help", 0, 0, 0) != PARAM_NOT_FOUND) { usage(); }
  if (get_param("bus", &bus, 0, 0) != PARAM_SUCCESS || bus < 0) {
    fprintf(stderr, "Missing spi bus number\n");
    usage();
  }
  if (get_param("cs", &cs, 0, 0) != PARAM_SUCCESS || cs < 0) {
    fprintf(stderr, "Missing spi cs number\n");
    usage();
  }
  spi_bus_len = 1 + snprintf(spi_bus_name, sizeof(spi_bus_name), "spi%li.%li", bus, cs);
  char str[64];
  snprintf(str, sizeof(str), "/dev/spidev%li.%li", bus, cs);
  fd = open(str, O_RDWR);
  if (fd < 0) {
    bind_spidev();
    fd = open(str, O_RDWR);
    if (fd < 0) {
      fprintf(stderr, "Error openning %s: %s\n", str, strerror(errno));
      exit(1);
    }
  }

  uint32_t speed = 10000000;
  uint8_t  mode  = SPI_MODE_0;
  uint8_t  bits  = 8;
  if (
    ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ , &speed) == -1 ||
    ioctl(fd, SPI_IOC_WR_MODE         , &mode ) == -1 ||
    ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bits ) == -1
  ) {
    fprintf(stderr, "Error ioctl %s: %s\n", str, strerror(errno));
    exit(1);
  }
  return &spi;
}
