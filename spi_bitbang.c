#include "bitbang.h"
#include "main.h"
#include "spi.h"
#include <stdio.h>
#include <stdlib.h>

static struct bitbang const *bitbang;
static unsigned manual_cs;

static void write_byte(uint8_t val) {
  for (int i = 7; i >= 0; --i) {
    bitbang->set_mosi((val >> i) & 1);
    bitbang->set_sck(1);
    bitbang->set_sck(0);
  }
}

static uint8_t read_byte() {
  uint8_t ret = 0;

  for (int i = 7; i >= 0; --i) {
    ret = (ret << 1) | bitbang->get_miso();
    bitbang->set_sck(1);
    bitbang->set_sck(0);
  }
  return ret;
}

static void cmd(unsigned wcnt, unsigned rcnt, uint8_t const *warr, uint8_t *rarr) {
  if (!manual_cs) { bitbang->set_cs(0); }
  for (unsigned i = 0; i < wcnt; ++i) { write_byte(warr[i]); }
  for (unsigned i = 0; i < rcnt; ++i) { rarr[i] = read_byte(); }
  if (!manual_cs) { bitbang->set_cs(1); }
}

//static void cs(unsigned cs) {
//  manual_cs = !cs;
//  bitbang->set_cs(cs);
//}

static const struct spi spi = {
  .cmd = cmd,
//.cs  = cs ,
};

static void usage(void) {
  fprintf(stdout,
    "programmer parameter should be: bitbang,backend,...\n"
    "    Where backend is one of: linux\n"
  );
  exit(1);
}

struct spi const *spi_bitbang(void) {
  if (get_param("linux", 0, 0, 0) == PARAM_NO_VALUE) {
    bitbang = bitbang_linux();
  }
  if (get_param("help", 0, 0, 0) != PARAM_NOT_FOUND) { usage(); }
  if (!bitbang) { return 0; }
  bitbang->set_cs  (1);
  bitbang->set_sck (0);
  bitbang->set_mosi(0);
  return &spi;
}
