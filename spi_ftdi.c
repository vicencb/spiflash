#include "main.h"
#include "spi.h"
#include <ftdi.h>
#include <libusb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
  CK     is bit 0.
  DO     is bit 1.
  DI     is bit 2.
  CS     is at cs from 3 to 7. Default is bit 3.
  GPIOL0 is at bit 4
  GPIOL1 is at bit 5
  GPIOL2 is at bit 6
  GPIOL3 is at bit 7
*/

static unsigned direction = 0x03;
static unsigned cs        = 0x08;
static unsigned value     = 0x00;
static struct ftdi_context ftdi;

static int set_cs(int val) {
  uint8_t buf[] = {
    SET_BITS_LOW,
    val ? cs | value : value,
    direction
  };
  return ftdi_write_data(&ftdi, buf, sizeof(buf)) != sizeof(buf);
}

static void cmd(unsigned wcnt, unsigned rcnt, uint8_t const *warr, uint8_t *rarr) {
  uint8_t buf[4];
  if (set_cs(0)) { goto err; }
  if (wcnt) {
    buf[0] = MPSSE_DO_WRITE | MPSSE_WRITE_NEG;
    buf[1] = ((wcnt - 1) >> 0) & 0xFF;
    buf[2] = ((wcnt - 1) >> 8) & 0xFF;
    if (ftdi_write_data(&ftdi, buf ,    3) !=         3) { goto err; }
    if (ftdi_write_data(&ftdi, warr, wcnt) != (int)wcnt) { goto err; }
  }
  if (rcnt) {
    buf[0] = MPSSE_DO_READ;
    buf[1] = ((rcnt - 1) >> 0) & 0xFF;
    buf[2] = ((rcnt - 1) >> 8) & 0xFF;
    if (ftdi_write_data(&ftdi, buf, 3) != 3) { goto err; }
    while (rcnt) {
      int r = ftdi_read_data(&ftdi, rarr, rcnt);
      if (r < 0) { goto err; }
      rcnt -= r;
      rarr += r;
    }
  }
  if (set_cs(1)) { goto err; }
  if (0) {
err:
    fprintf(stderr, "spi_cmd error: %s\n", ftdi_get_error_string(&ftdi));
    exit(1);
  }
}

static const struct spi spi = {
  .cmd = cmd,
};

static void close_ftdi(void) {
  ftdi_set_bitmode(&ftdi, 0, BITMODE_RESET);
  ftdi_usb_close  (&ftdi);
  ftdi_deinit     (&ftdi);
}

static void usage(void) {
  fprintf(stdout,
    "programmer parameter should be: ftdi[,vid=0x0403][,pid=0x6010][,desc=Dual RS232-HS][,serial=abcde][,index=0][,intf=A][,cs=3][,l0=0]\n"
    "    Where vid and pid can be set to the USB Vendor Identifier and USB Product Identifier.\n"
    "      If not specified default values shown above are used.\n"
    "    desc, serial and index are used to select a specific device in case multiple are found.\n"
    "    Set intf to A, B, C or D to select the interface on multi-port devices.\n"
    "    If your programmer has CS connected to a non-standard location, set cs to 4 for GPIOL0, ..., 7 for GPIOL3\n"
    "    If your programmer needs specific values on GPIOL0, ..., GPIOL3, set them into l0, ..., l3.\n"
    "    If your programmer needs specific values on GPIOH0, ..., GPIOH7, set them into h0, ..., h7.\n"
    "    Example: for a KT-LINK compatible interface set l1=1,h4=0,h5=0,h6=0.\n"
    "    Example: for a JTAG-KEY compatible interface set l0=0.\n"
  );
  exit(1);
}

struct spi const *spi_ftdi(void) {
  long            vid    = 0x0403;
  long            pid    = 0x6010;
  long            intfn  = INTERFACE_A;
  char            intfc  = 'A';
  char           *intfs  = 0;
  char           *desc   = 0;
  char           *serial = 0;
  long            index  = 0;
  long            csi    = 0;
  uint8_t         buf[]  = {TCK_DIVISOR, 0, 0}; // 6MHz
  unsigned        high_val = 0;
  unsigned        high_dir = 0;
  unsigned        len;
  enum param_code pc;

  if (get_param("help", 0, 0, 0) != PARAM_NOT_FOUND) { usage(); }
  pc = get_param("vid", &vid, 0, 0);
  if (pc != PARAM_NOT_FOUND && (pc != PARAM_SUCCESS || vid < 0 || vid >= 0x10000)) {
    fprintf(stderr, "Invalid vid number\n");
    usage();
  }
  pc = get_param("pid", &pid, 0, 0);
  if (pc != PARAM_NOT_FOUND && (pc != PARAM_SUCCESS || pid < 0 || pid >= 0x10000)) {
    fprintf(stderr, "Invalid pid number\n");
    usage();
  }
  pc = get_param("desc", 0, &desc, &len);
  if (pc != PARAM_NOT_FOUND && pc != PARAM_SUCCESS) {
    fprintf(stderr, "Invalid desc\n");
    usage();
  }
  if (desc) { desc = strndup(desc, len); }
  pc = get_param("serial", 0, &serial, &len);
  if (pc != PARAM_NOT_FOUND && pc != PARAM_SUCCESS) {
    fprintf(stderr, "Invalid serial\n");
    usage();
  }
  if (serial) { serial = strndup(serial, len); }
  pc = get_param("index", &index, 0, 0);
  if (pc != PARAM_NOT_FOUND && (pc != PARAM_SUCCESS || index < 0 || index != (unsigned)index)) {
    fprintf(stderr, "Invalid index number\n");
    usage();
  }
  len = 0;
  pc = get_param("intf", 0, &intfs, &len);
  if (len == 1) {
    intfc = intfs[0] & ~' ';
    intfn = intfc - 'A' + INTERFACE_A;
  }
  if (pc != PARAM_NOT_FOUND && (pc != PARAM_SUCCESS || len != 1 || intfn < INTERFACE_A || intfn > INTERFACE_D)) {
    fprintf(stderr, "Invalid intf\n");
    usage();
  }
  pc = get_param("cs", &csi, 0, 0);
  if (pc != PARAM_NOT_FOUND && (pc != PARAM_SUCCESS || csi < 3 || csi >= 8)) {
    fprintf(stderr, "Invalid cs number. Valid range is from 3 to 7.\n");
    usage();
  }
  if (csi) { cs = 1u << csi; }
  direction |= cs;
  for (unsigned i = 0; i < 4; ++i) {
    char     param[4] = {'l', i + '0'};
    unsigned bit      = i + 4;
    long     gpiol;
    pc = get_param(param, &gpiol, 0, 0);
    if (pc == PARAM_NOT_FOUND) { continue; }
    if (pc != PARAM_SUCCESS || gpiol < 0 || gpiol > 1) {
      fprintf(stderr, "Invalid GPIOL%u bit value\n", i);
      usage();
    }
    if ((cs >> bit) == 1) {
      fprintf(stderr, "GPIOL%u bit is used as CS\n", i);
      usage();
    }
    direction |= 1u    << bit;
    value     |= gpiol << bit;
  }
  for (unsigned bit = 0; bit < 8; ++bit) {
    char param[4] = {'h', bit + '0'};
    long gpioh;
    pc = get_param(param, &gpioh, 0, 0);
    if (pc == PARAM_NOT_FOUND) { continue; }
    if (pc != PARAM_SUCCESS || gpioh < 0 || gpioh > 1) {
      fprintf(stderr, "Invalid GPIOH%u bit value\n", bit);
      usage();
    }
    high_val |= gpioh << bit;
    high_dir |= 1u    << bit;
  }

  if (ftdi_init               (&ftdi                                 )) { goto err; }
  if (ftdi_set_interface      (&ftdi, intfn                          )) { goto err; }
  if (ftdi_usb_open_desc_index(&ftdi, vid, pid, desc, serial, index  )) { goto err; }
  atexit(close_ftdi);
  if (ftdi_usb_reset          (&ftdi                                 )) { goto err; }
  if (ftdi_set_latency_timer  (&ftdi, 1                              )) { goto err; }
  if (ftdi_set_bitmode        (&ftdi, 0, BITMODE_RESET               )) { goto err; }
  if (ftdi_set_bitmode        (&ftdi, 0, BITMODE_MPSSE               )) { goto err; }
  if (ftdi_write_data         (&ftdi, buf, sizeof(buf)) != sizeof(buf)) { goto err; }
  buf[0] = SET_BITS_LOW;
  buf[1] = cs | value;
  buf[2] = direction;
  if (ftdi_write_data         (&ftdi, buf, 3          ) != 3          ) { goto err; }
  buf[0] = SET_BITS_HIGH;
  buf[1] = high_val;
  buf[2] = high_dir;
  if (ftdi_write_data         (&ftdi, buf, 3          ) != 3          ) { goto err; }

  return &spi;

  if (0) {
err:
    fprintf(stderr, "Error with %04lX:%04lX:%c '%s' '%s': %s\n", vid, pid, intfc, desc, serial, ftdi_get_error_string(&ftdi));
    exit(1);
  }
}
