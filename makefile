FTDI ?= $(shell pkg-config libftdi1 && echo yes)

CFLAGS+=-Wall
CFLAGS+=-Wextra
CFLAGS+=-Werror
CFLAGS+=-Os

STRIP ?= ${CROSS_COMPILE}strip

HDR+=main.h
HDR+=flash.h
HDR+=spi.h
HDR+=bitbang.h

SRC+=main.c
SRC+=flash_spi.c
SRC+=spi_linux.c
SRC+=spi_bitbang.c
SRC+=bitbang_linux.c

ifeq (${FTDI},yes)
CFLAGS+=-DFTDI=${FTDI}
CFLAGS+=$$(pkg-config --cflags libftdi1)
LDFLAGS+=$$(pkg-config --libs libftdi1)
SRC+=spi_ftdi.c
endif

FLASH_FILE?=flash.img

spiflash : spiflash.unstripped ; ${STRIP} -sR .comment -o $@ $<
spiflash.unstripped : ${SRC} ${HDR} makefile ; ${CC} ${CFLAGS} ${SRC} ${LDFLAGS} -o $@

clean : ; rm -f spiflash spiflash.unstripped
.PHONY : clean

# RK3399
BBL_PROG = "bitbang,linux,cs=42,clk=41,mosi=40,miso=39"
# RK3328
# BBL_PROG = "bitbang,linux,cs=104,clk=96,mosi=97,miso=98"

gi : spiflash ; sudo ./spiflash -p ${BBL_PROG}
gr : spiflash ; sudo ./spiflash -p ${BBL_PROG}    -r ${FLASH_FILE}
gv : spiflash ; sudo ./spiflash -p ${BBL_PROG} -o -v ${FLASH_FILE}
gw : spiflash ; sudo ./spiflash -p ${BBL_PROG} -o -w ${FLASH_FILE}
ge : spiflash ; sudo ./spiflash -p ${BBL_PROG}    -e
.PHONY : gi gr gv gw ge

si : spiflash ; sudo ./spiflash -p linux,bus=0,cs=0
sr : spiflash ; sudo ./spiflash -p linux,bus=0,cs=0    -r ${FLASH_FILE}
sv : spiflash ; sudo ./spiflash -p linux,bus=0,cs=0 -o -v ${FLASH_FILE}
sw : spiflash ; sudo ./spiflash -p linux,bus=0,cs=0 -o -w ${FLASH_FILE}
se : spiflash ; sudo ./spiflash -p linux,bus=0,cs=0    -e
.PHONY : si sr sv sw se

# l0=0 is required by JTAG-KEY compatible programmers like dpbusblaster
fi : spiflash ; ./spiflash -p ftdi,l0=0
fn : spiflash ; ./spiflash -p ftdi,l0=0    -r ${FLASH_FILE} -n 0x20000
fr : spiflash ; ./spiflash -p ftdi,l0=0    -r ${FLASH_FILE}
fv : spiflash ; ./spiflash -p ftdi,l0=0 -o -v ${FLASH_FILE}
fw : spiflash ; ./spiflash -p ftdi,l0=0 -o -w ${FLASH_FILE}
fe : spiflash ; ./spiflash -p ftdi,l0=0    -e
.PHONY : fi fr fv fw fe
