#ifndef BITBANG_H
#define BITBANG_H

struct bitbang {
  void (*set_cs  ) (int val);
  void (*set_sck ) (int val);
  void (*set_mosi) (int val);
  int  (*get_miso) (void   );
};

struct bitbang const *bitbang_linux(void);

#endif
