#include "flash.h"
#include "main.h"
#include "spi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static struct spi const *spi;
static struct flash_sfdp sfdp;

enum {
  JEDEC_REMS  = 0x90,
  JEDEC_RDID  = 0x9F,
  JEDEC_SFDP  = 0x5A,
  JEDEC_RDSR  = 0x05,
  JEDEC_READ  = 0x03,
  JEDEC_WREN  = 0x06,
  JEDEC_WRDI  = 0x04,
  JEDEC_WRITE = 0x02,
};

struct flash_sfdp_header {
  uint32_t signature;
  uint8_t  minor;
  uint8_t  major;
  uint8_t  num_param_headers;
};

struct flash_param_header {
  uint8_t  id   ;
  uint8_t  minor;
  uint8_t  major;
  uint8_t  size ;
  uint32_t ptr  :24;
};

struct flash_jedec_header {
  uint32_t ERASE_4KB   : 2;
  uint32_t             : 6;
  uint32_t ERASE_4KB_OP: 8;
  uint32_t             : 1;
  uint32_t ADDR_BYTES  : 2;
  uint32_t             :13;
  uint32_t N           ;
};

struct flash_header {
  struct flash_sfdp_header  sfdp;
  struct flash_param_header param;
};

static unsigned id(void) {
  uint8_t buf[4] = {JEDEC_REMS};
  spi->cmd(4, 2, buf, buf);
  unsigned ma1 = buf[0];
  unsigned dev = buf[1];
  buf[0] = JEDEC_RDID;
  spi->cmd(1, 3, buf, buf);
  unsigned ma2 = buf[0];
  unsigned typ = buf[1];
  unsigned cap = buf[2];
  if (ma1 != ma2) {
    fprintf(stderr, "Mismatched manufacturer ID 0x%02X != 0x%02X\n", ma1, ma2);
    exit(1);
  }
  return (ma1 << 24) | (dev << 16) | (typ << 8) | cap;
}

static unsigned status(void) {
  uint8_t buf = JEDEC_RDSR;
  spi->cmd(1, 1, &buf, &buf);
  return buf;
}

static void read(unsigned addr, uint8_t *dat) {
  uint8_t buf[4] = {JEDEC_READ, addr >> 16, (addr >> 8) & 0xFF, addr & 0xFF};
#ifdef DEBUG
  // -fsanitize=memory reports a false positive
  memset(dat, 0xEE, FLASH_BLOCK_SIZE);
#endif
  spi->cmd(4, FLASH_BLOCK_SIZE, buf, dat);
}

static void write(unsigned addr, uint8_t const *dat) {
  uint8_t buf[4 + FLASH_PAGE_SIZE];
  buf[0] = JEDEC_WRITE;
  buf[1] =  addr >> 16;
  buf[2] = (addr >>  8) & 0xFF;
  buf[3] =  addr        & 0xFF;
  memcpy(buf + 4, dat, FLASH_PAGE_SIZE);
  spi->cmd(sizeof(buf), 0, buf, 0);
}

static void erase(unsigned addr) {
  uint8_t buf[4] = {sfdp.erase_op, addr >> 16, (addr >> 8) & 0xFF, addr & 0xFF};
  spi->cmd(4, 0, buf, 0);
}

static void wren(unsigned en) {
  uint8_t buf = en ? JEDEC_WREN : JEDEC_WRDI;
  spi->cmd(1, 0, &buf, 0);
}

static struct flash_sfdp const *read_sfdp(void) {
  union {
    uint8_t                   arr[8];
    struct flash_header       header;
    struct flash_jedec_header jedec ;
  } buf = {.arr = {JEDEC_SFDP}};

  spi->cmd(5, sizeof(struct flash_header), buf.arr, buf.arr);
  if (buf.header.sfdp.signature != 0x50444653) {
    fprintf(stderr, "Invalid sfdp signature 0x%08X\n", buf.header.sfdp.signature);
    exit(1);
  }
  if (buf.header.sfdp.major != 1) {
    fprintf(stderr, "Unsupported sfdp major 0x%02X\n", buf.header.sfdp.major);
    exit(1);
  }
  if (buf.header.param.id != 0) {
    fprintf(stderr, "Invalid JEDEC ID number 0x%02X\n", buf.header.param.id);
    exit(1);
  }
  if (buf.header.param.major != 1) {
    fprintf(stderr, "Unsupported jedec major 0x%02X\n", buf.header.param.major);
    exit(1);
  }
  if (buf.header.param.size < 1) {
    fprintf(stderr, "Unsupported jedec size 0x%02X\n", buf.header.param.size);
    exit(1);
  }

  buf.arr[0] = JEDEC_SFDP;
  buf.arr[1] =  buf.header.param.ptr >> 16;
  buf.arr[2] = (buf.header.param.ptr >>  8) & 0xFF;
  buf.arr[3] =  buf.header.param.ptr        & 0xFF;
  buf.arr[4] = 0;
  spi->cmd(5, sizeof(struct flash_jedec_header), buf.arr, buf.arr);

  sfdp.erase_op = buf.jedec.ERASE_4KB_OP;
  sfdp.size     = (buf.jedec.N + 1) / 8;
  if (buf.jedec.ADDR_BYTES > 1) {
    fprintf(stderr, "Unsupported address %u\n", buf.jedec.ADDR_BYTES);
    exit(1);
  }
  if (buf.jedec.ERASE_4KB != 1) {
    fprintf(stderr, "Unsupported 4KB erase %u\n", buf.jedec.ERASE_4KB);
    exit(1);
  }
  if (sfdp.size < FLASH_BLOCK_SIZE || sfdp.size > 0x1000000 || (sfdp.size & (sfdp.size - 1))) {
    fprintf(stderr, "Unsupported size 0x%08X\n", sfdp.size);
    exit(1);
  }

  return &sfdp;
}

static const struct flash flash = {
  .id      = id       ,
  .status  = status   ,
  .sfdp    = read_sfdp,
  .read    = read     ,
  .write   = write    ,
  .wren    = wren     ,
  .erase   = erase    ,
};

struct flash const *flash_spi(void) {
  if (get_param("bitbang", 0, 0, 0) == PARAM_NO_VALUE) {
    spi = spi_bitbang();
  } else if (get_param("linux", 0, 0, 0) == PARAM_NO_VALUE) {
    spi = spi_linux();
#ifdef FTDI
  } else if (get_param("ftdi", 0, 0, 0) == PARAM_NO_VALUE) {
    spi = spi_ftdi();
#endif
  }
  if (!spi) { return 0; }
  return &flash;
}
