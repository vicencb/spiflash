#ifndef SPI_H
#define SPI_H

#include <stdint.h>

struct spi {
  void (*cmd)(unsigned wcnt, unsigned rcnt, uint8_t const *warr, uint8_t *rarr);
//void (*cs )(unsigned);
};

struct spi const *spi_bitbang(void);
struct spi const *spi_linux(void);
struct spi const *spi_ftdi(void);

#endif
